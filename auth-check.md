# GET `/order-ext/{ORDER_NUMBER}`
Возвращает информацию по заказу

## Параметры

| Ключ            | Тип значения | Описание          |
|-----------------|--------------|-------------------|
| token           | string       | Токен авторизации |
| {ORDER_NUMBER}  | string       | номер заказа      |

```json
{
    token: "X-AAAAZZZZDDDDBBBB",
}
```

## Ответ
Возвращает информацию о заказе

| Ключ                | Тип значения | Описание                                             |
|---------------------|--------------|------------------------------------------------------|
| orderNumber         | string       | номер заказа                                         |
| sender              | object       | информация об отправителе                            |
| receiver            | object       | информация о получателе                              |
| pickupDate          | date         | дата отправления (формат YYYY-MM-DD hh:mm:ss)        |
| planDeliveryDate    | date         | примерная дата доставки (формат YYYY-MM-DD)          |
| orderPhysicalWeight | float        | физ. вес заказа                                      |
| orderCost           | decimal      | стоимость доставки                                   |
| description         | text         | описание состава отправления                         |
| events              | array        | массив статусов заказа                               |
| events.code         | string       | код статуса                                          |
| events.name         | string       | наименование статус                                  |
| events.date         | date         | дата изменения статуса (формат YYYY-MM-DD hh:mm:ss)  |


```json
{
    "orderNumber": "RU1234567890",
    "sender": {
        "address": "г. Москва, ул. Ленина, 31",
        "firstName": "Иван",
        "lastName": "Иванов",
    },
    "receiver": {
        "address": "г. Тула, ул. Ленина, 31",
        "firstName": "Иван",
        "lastName": "Иванов",
    },
    "pickupDate": "2019-08-01 12:31:23",
    "planDeliveryDate": "2019-08-03",
    "orderPhysicalWeight": 1.2,
    "orderCost": 123.85,
    "description": "Телефон IPhone X",
    "events": [
        {
            "code": "OrderCreate",
            "name": "Заказ создан",
            "date": "2019-08-01 12:31:23"
        },

        {
            "code": "OrderPickup",
            "name": "Заказ принят у отправителя",
            "date": "2019-08-01 12:31:23"
        },
    ]
}
```